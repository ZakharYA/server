# Настройка ci/cd на базе gitlab + docker compose

Подразумевается, что у специалиста, который занимается настройкой, должно быть хотя бы общее понимание, как работает CI/CD в Gitlab.

> **Внимание!!!** Прежде чем устанавливать gitlab-раннер на сервере в варианте shell executor убедитесь в безопасности данного решения для вас. Подробнее см. в официальной документации по ссылке: https://docs.gitlab.com/runner/executors/shell.html#security. 

## 1. Установка gitlab-runner на сервере

Установить gitlab-runner по инструкции, не регистрируя runner на этом этапе:
https://docs.gitlab.com/runner/install/.

## 2. Смена пользователя на bitrix

Запуск деплоя нужно выполнять под пользователем на хосте, под которым предполагается редактирование файлов.
Для стандартизации рекомендуется использовать пользователя `bitrix`.

```bash
gitlab-runner uninstall
gitlab-runner install --working-directory /home/bitrix --user bitrix
# проверяем, что настройки применились
cat /etc/systemd/system/gitlab-runner.service
# далее перезапускаемся:
reboot
systemctl daemon-reload
```

## 3. Регистрация gitlab-runner

https://docs.gitlab.com/runner/register/index.html

> **Важно!!!**
> - Задать тег, по которому далее будут настраиваться задания, чтобы они выполнялись именно этим раннером на этом сервере. Чтобы не путаться в тегах - лучше использовать имя домена (тестового или боевого), где точки заменить на дефисы. 
> - На вопрос `Enter an executor` указать вариант shell, чтобы раннер запускался непосредственно на самом сервере.

## 4. Настройка заданий

1. Скопировать [bitrix-distr/.gitlab-ci.example.yml](../bitrix-distr/.gitlab-ci.example.yml) в .gitlab-ci.yml в корне сайта.
2. В комплекте в папке [bitrix-distr/ci/job-templates](../bitrix-distr/ci/job-templates) приведены основные шаблоны заданий, которые могут пригодиться для деплоя проекта на Битрикс.
